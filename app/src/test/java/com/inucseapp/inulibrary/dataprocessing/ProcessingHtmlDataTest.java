package com.inucseapp.inulibrary.dataprocessing;

import com.inucseapp.inulibrary.domain.Book;
import com.inucseapp.inulibrary.domain.BookSearchCondition;
import com.inucseapp.inulibrary.domain.SitePage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by junha on 2017. 2. 27..
 */


public class ProcessingHtmlDataTest {

//    @Ignore
//    @Test
//    public void splitBookAreaTest(){
//        ProcessingHtmlData processingHtmlDataTest = new ProcessingHtmlData();
//        String originUrl = "http://lib.inu.ac.kr/search/tot/result?q=글쓰기+&st=KWRD&y=0&si=TOTAL&x=0&oi=DISP06&os=DESC&cpp=10";
//
//        ParsingHtmlThread parsingHtml = new ParsingHtmlThread(originUrl);
//
//        parsingHtml.start();
//
//        try{
//            parsingHtml.join();
//        }catch(Exception e){
//            e.printStackTrace();;
//        }
//
//        String parsingResult = parsingHtml.getHtml().toString();
//
//        //System.out.println(parsingResult);
//        //<formname="briefFrm"method="post"action="/search/brief/service"onsubmit="returnchecked(this);">
//        String pattern = String.format("<formname=\"briefFrm\"method=\"post\"action=\"/search/brief/service\"onsubmit=\"returnchecked\\(this\\);\">.*<formname=\"frmSer\"");
//
//        Pattern areaPattern = Pattern.compile(pattern);
//        Matcher areaPatternMatches = areaPattern.matcher(parsingResult);
//
//        String result = "";
//        String strTemp1 = "";
//
//        while(areaPatternMatches.find()){
//            strTemp1 = areaPatternMatches.group(0).trim()
//                            .replace("<formname=\"briefFrm\"method=\"post\"action=\"/search/brief/service\"onsubmit=\"returnchecked(this);\">","")
//                            .replace("<formname=\"frmSer\"","");
//            System.out.println(strTemp1);
//        }
//        //String trimResult = processingHtmlDataTest.splitBookArea(parsingHtml.DownloadHtml(originUrl));
//
//        //System.out.println(trimResult);
//    }


    @Ignore
    @Test
    public void splitBrookInfoAreaTest_실행하면_책정보부분만_잘려나온다(){
        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        ParsingHtmlThread parsingHtml = new ParsingHtmlThread();

        List<String> bookHtmlList = new ArrayList<>();
        List<Book> bookList = new ArrayList<>();

        String basicUrl = "http://lib.inu.ac.kr/search/tot/result?";

        BookSearchCondition bookSearchCondition = new BookSearchCondition();

        bookSearchCondition.setPageNumber("1");
        bookSearchCondition.setKeyword("해리포터와 불의 잔");
        bookSearchCondition.setSearchType("KWRD");
        bookSearchCondition.setY("0");
        bookSearchCondition.setSearchArrange("TOTAL");
        bookSearchCondition.setX("0");
        bookSearchCondition.setSortMethod("DISP06");
        bookSearchCondition.setSort("DESC");
        bookSearchCondition.setResultNum("10");


        String regex1 = "<form name=\"briefFrm\" method=\"post\" action=\"/search/brief/service\" onsubmit=\"return checked\\(this\\);\" >.*<form name=\"frmSer\"";

        String regex = "<form name=\"briefFrm\" method=\"post\" action=\"\\/search\\/brief\\/service\" onsubmit=\"return checked\\(this\\);\">.*<form name=\"frmSer\"";

        Document document = null;
        StringBuilder html = new StringBuilder();

        System.out.println(basicUrl + bookSearchCondition.toString());

        try{
            document = Jsoup.connect(basicUrl + bookSearchCondition.toString()).timeout(15000).get();
            html.append(document.html());
        }catch(Exception e){
            e.printStackTrace();
        }

        String splitArea = processingHtmlData.splitArea( html.toString()
                                                             .replaceAll("[\\n\\r\\t]", "")
                                                             .replaceAll(">\\s+", ">")
                                                             .replaceAll("\\s+<","<"), regex );

        bookHtmlList = processingHtmlData.getBookInfoArea(splitArea);

        for(int i=0; i < bookHtmlList.size(); i++){
            System.out.println(i + " : " + bookHtmlList.get(i));
        }

        for(int i=0; i<bookHtmlList.size(); i++){
            Book book = processingHtmlData.getBookInfoFromSearchPage(bookHtmlList.get(i));
            bookList.add(book);
        }

        for(int i=0; i<bookHtmlList.size(); i++){
            System.out.println(bookList.get(i).getBookTitle());
            System.out.println(bookList.get(i).getAuthor());
            System.out.println(bookList.get(i).getPublisher());
            System.out.println(bookList.get(i).getBookID());
            System.out.println(bookList.get(i).getIssueYear());
            System.out.println(bookList.get(i).getBookUrl() + "\n");

        }
        //<dd class="searchTitle">.</dd>
    }

    @Ignore
    @Test
    public void splitPageInfo_현재_페이지_정보를_가져온다(){

        String basicUrl = "http://lib.inu.ac.kr/search/tot/result?";

        BookSearchCondition bookSearchCondition = new BookSearchCondition();
        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();

        bookSearchCondition.setPageNumber("6");
        bookSearchCondition.setKeyword("안드로이드");
        bookSearchCondition.setSearchType("KWRD");
        bookSearchCondition.setY("0");
        bookSearchCondition.setSearchArrange("TOTAL");
        bookSearchCondition.setX("0");
        bookSearchCondition.setSortMethod("DISP06");
        bookSearchCondition.setSort("DESC");
        bookSearchCondition.setResultNum("10");

        Document document = null;
        StringBuilder html = new StringBuilder();

        System.out.println(basicUrl + bookSearchCondition.toString());

        try{
            document = Jsoup.connect(basicUrl + bookSearchCondition.toString()).timeout(15000).get();
            html.append(document.html());
        }catch(Exception e){
            e.printStackTrace();
        }

        String modifiedHtml = html.toString().replaceAll("[\\n\\r\\t]", "").replaceAll(">\\s+", ">") .replaceAll("\\s+<","<");

        //<strong>\d\/\d+<\/strong>페이지
        String result = processingHtmlData.splitArea(modifiedHtml, "<strong>\\d+\\/\\d+<\\/strong>페이지").replaceAll("<\\/?strong>","").replaceAll("페이지","");

        SitePage sitePage = new SitePage();

        //processingHtmlData.getPageInfo(result, sitePage);

        //System.out.println("current : " + sitePage.getBookCurrentPage() + " total : " + sitePage.getBookTotalPage());

        //int pageinfo = processingHtmlData.getCurrentPage(html.toString().replaceAll("[\\n\\r\\t]", "").replaceAll(">\\s+", ">") .replaceAll("\\s+<","<"));

    }

    @Ignore
    @Test
    public void splitBookAreaTest_실행하면_책정보_구역이_잘려나옴(){

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        ParsingHtmlThread parsingHtml = new ParsingHtmlThread();

        String basicUrl = "http://lib.inu.ac.kr/search/tot/result?";

        List<String> resultList = new ArrayList<>();

        BookSearchCondition bookSearchCondition = new BookSearchCondition();

        bookSearchCondition.setKeyword("해리포터와 불의 잔");
        bookSearchCondition.setSearchType("KWRD");
        bookSearchCondition.setY("0");
        bookSearchCondition.setSearchArrange("TOTAL");
        bookSearchCondition.setX("0");
        bookSearchCondition.setSortMethod("DISP06");
        bookSearchCondition.setSort("DESC");
        bookSearchCondition.setResultNum("10");

        for(int i = 0; i<Integer.parseInt(bookSearchCondition.getResultNum()); i++){

            String regex1= "<div id=\"dataInfo" + i + "\".*class=\"possessview";

            /*if(i == Integer.parseInt(bookSearchCondition.getResultNum())-1 ){
                regex1 = "<div id=\"dataInfo" + i + "\".*<div id=\"divPaging\"";
            }*/

            String bookInfoArea = processingHtmlData.splitArea( parsingHtml.DownloadHtml(basicUrl + bookSearchCondition.toString()), regex1 );

            resultList.add(bookInfoArea);

        }

        for(int i=0; i<resultList.size(); i++) {
            System.out.println(resultList.get(i));
        }



    }

    @Ignore
    @Test
    public void getBookInfoTest(){
        String test = "<div id=\"dataInfo0\" class=\"briefData first-child\"><div class=\"brief\"><span class=\"check\"><input type=\"checkbox\" name=\"data\" value=\"CAT000000366254\" class=\"checkbox\" title=\"checkbox\"></span><span class=\"linenum\">1.</span><span id=\"bookImg_CATTOT000000366254\" class=\"bookimg\"></span><dl class=\"briefDetail\"><dt class=\"skip\">서명</dt><dd class=\"searchTitle\"><a href=\"/search/detail/CATTOT000000366254?mainLink=/search/tot&amp;briefLink=/search/tot/result?os=DESC_A_cpp=10_A_q=%EC%9A%B4%EC%98%81%EC%B2%B4%EC%A0%9C_A_st=KWRD_A_oi=DISP01_A_y=0_A_x=0_A_si=TOTAL_A_pn=1\">현대<span class=\"hilight\">운영체제</span>론: 이론과 실습</a>&nbsp;<img id=\"preview_open_CATTOT000000366254\" alt=\"미리보기\" title=\"미리보기\" src=\"/image/ko/common/ico/advance.gif\" onclick=\"preview('CATTOT000000366254')\" width=\"14\" height=\"13\" style=\"cursor: pointer;\"><img id=\"preview_close_CATTOT000000366254\" alt=\"미리보기닫기\" title=\"미리보기닫기\" src=\"/image/ko/common/ico/advance_on.gif\" onclick=\"preview('CATTOT000000366254')\" width=\"14\" height=\"13\" style=\"display:none;cursor: pointer;\"></dd><dt class=\"skip\">저자</dt><dd class=\"bookline\">송주석</dd><dt class=\"skip\">출판사</dt><dd class=\"bookline\">사이텍미디어</dd><dt class=\"skip\">청구기호</dt><dd class=\"bookline\">005.43 송77ㅎ</dd><dt class=\"skip\">발행년</dt><dd class=\"bookline\">1998</dd><dt class=\"skip\">자료유형</dt><dd class=\"bookline\"><img alt=\"단행본\" title=\"단행본\" src=\"/image/ko/common/ico/type_m.gif\" width=\"25\" height=\"20\">단행본</dd><dt class=\"skip\">소장처</dt><dd class=\"bookline\"><a href=\"#callLocation\" class=\"briefDeFont\" onkeydown=\"javascript:callLocation('0','1','CAT000000366254','F0000001')\" onmousedown=\"javascript:callLocation('0','1','CAT000000366254','F0000001')\">학산도서관<span class=\"book_state\">대출가능</span><img id=\"locImg_CAT0000003662540\" alt=\"열기\" src=\"/image/ko/common/ico/down.gif\"></a></dd></dl></div><div id=\"prevData_CATTOT000000366254\" style=\"display:none;\" class=\"searchPreview\"></div><div id=\"prevLoc_CAT0000003662540\" style=\"display:none;\" class=\"possessview\"></div></div><div id=\"dataInfo1\"";
        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();

        Book result = processingHtmlData.getBookInfoFromSearchPage(test);

        System.out.println(result.getBookTitle());
    }

    @Ignore
    @Test
    public void splitAreaTest_실행하면_자세한_도서정보가_나온다(){

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        ParsingHtmlThread parsingHtml = new ParsingHtmlThread();

        List<String> resultList = new ArrayList<>();


        String regex = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><p><\\/p>";
        String basicUrl = "http://lib.inu.ac.kr/search/detail/CATTOT000000340397?mainLink=/search/tot&briefLink=/search/tot/result?q=%ED%95%B4%EB%A6%AC%ED%8F%AC%ED%84%B0_A_st=KWRD_A_si=TOTAL";

        String basicUrl1 = "http://lib.inu.ac.kr/search/detail/CATTOT000000550858?mainLink=/search/tot&briefLink=/search/tot/result?q=DRAM_A_st=KWRD_A_y=0_A_x=0_A_si=TOTAL";

        String parsingResult = parsingHtml.DownloadHtml(basicUrl1);

        String splitArea = processingHtmlData.splitArea(parsingResult, regex);

        System.out.println("splitArea : " + splitArea);

        int index = 1;
        System.out.println("<td>"+ (index) +"<\\/td>.*<td>"+ (index+1) +"<\\/td>");

        String bookinfoArea = processingHtmlData.splitArea(splitArea, "<td>"+ (index) +"<\\/td>.*<td>"+ (index+1) +"<\\/td>");

        //26을 조회했을 때 안나왔음 이떄는 regex를 다시 설정해서 검색해야함
        index = 1;

        //System.out.println("<td>"+ (index) +"<\\/td>.*</a><a class=\"service\"");
        System.out.println(splitArea);
        String lastResult = processingHtmlData.splitArea(splitArea, "<td>"+ (index) +"<\\/td>.*<td><div style=\"display");
        //String lastResult = processingHtmlData.splitArea(splitArea, "<td>"+ (index) +"<\\/td>.*</td><td><a\\shref");

        System.out.println("result : " + bookinfoArea);
        System.out.println("last : " + lastResult);

    }


    @Test
    public void splitAreaTest_실행하면_도서리스트가_나온다(){

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        ParsingHtmlThread parsingHtml = new ParsingHtmlThread();

        List<String> resultList = new ArrayList<>();

        String regex = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><script";
        String basicUrl = "http://lib.inu.ac.kr/search/detail/CATTOT000000760564";

        String parsingResult = parsingHtml.DownloadHtml(basicUrl);

        System.out.println(parsingResult);

        String splitArea = processingHtmlData.splitArea(parsingResult, regex);

        resultList = processingHtmlData.getBookDetailInfo(splitArea);

        //이 결과를 다시 분리해야함
        if(resultList == null) {
            System.out.println("전자책일 경우 null");
        }

        for(int i=0; i < resultList.size(); i++){
            Book book = new Book();
            //System.out.println("index : " + (i+1) + " , " + resultList.get(i));
            book = processingHtmlData.getBookInfoFromDetailPage(resultList.get(i));

            System.out.println(book.getIndex());
            System.out.println(book.getBookRegisterID());
            System.out.println(book.getBookID());
            System.out.println(book.getBookLocation());
            System.out.println(book.getBookState());
            System.out.println(book.getBookReturnDate());   //null이면 출력하지 않도록 설정해야함.
        }
    }

    @Ignore
    @Test
    public void testGetBookInfoFromDetailPage(){

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        Book book;

        String detail = "<td>1</td><td>EM298346</td><td>843 R884h최 v.5-1</td><td>사범대학교/자료실/</td><td class=\"point\">대출가능</td><td></td><td></td><td><a class=\"service\" href=\"/missrepo/write?accession_no=EM0000298346\"><img src=\"/image/ko/common/ico/missrepo.gif\" width=\"16\" height=\"16\" alt=\"소재불명도서\" title=\"소재불명도서\"></a><a class=\"service\" href=\"/search/handler/output?submit=PRINT&amp;brief=Y&amp;holding=Y&amp;posPrint=Y&amp;accession_no=EM0000298346&amp;data=CAT000000340397\" target=\"_blank\"><img src=\"/image/ko/common/ico/print.gif\" width=\"16\" height=\"16\" alt=\"인쇄 이미지\" title=\"인쇄\"></a>                                                                                    </td>                    <td><div style=\"display:none\">false|false|true|false |false|true |false</div>                                                                     &nbsp;                    </td></tr><tr><td>2</td>\n";

        book = processingHtmlData.getBookInfoFromDetailPage(detail);

        System.out.println(book.getIndex());
        System.out.println(book.getBookRegisterID());
        System.out.println(book.getBookID());
        System.out.println(book.getBookLocation());
        System.out.println(book.getBookState());
        System.out.println(book.getBookReturnDate());

    }


    /*@Ignore
    @Test
    public void testGetBookState_책상태를_가져와서_갯수를_센다(){

        Document document = null;
        StringBuilder html = new StringBuilder();

        String basicURL = "http://lib.inu.ac.kr/search/detail/";
        String bookURL = "CATTOT000000682079";

        System.out.println(basicURL + bookURL);

        try{
            document = Jsoup.connect(basicURL + bookURL).timeout(15000).get();
            html.append(document.html());
        }catch(Exception e){
            e.printStackTrace();
        }

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        ParsingHtmlThread parsingHtml = new ParsingHtmlThread();

        List<String> resultList = new ArrayList<>();
        List<Book> bookList = new ArrayList<>();

        String regex = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><p><\\/p>";
        String basicUrl = "http://lib.inu.ac.kr/search/detail/CATTOT000000760564";

        String splitArea = processingHtmlData.splitArea( html.toString()
                .replaceAll("[\\n\\r\\t]", "")
                .replaceAll(">\\s+", ">")
                .replaceAll("\\s+<","<"), regex );

        System.out.println(splitArea);

        resultList = processingHtmlData.getBookDetailInfo(splitArea);

        for(int i=0; i<resultList.size(); i++){

            Book book = new Book();

            book = processingHtmlData.getBookInfoFromDetailPage(resultList.get(i));
            bookList.add(book);

        }

        for(int i = 0; i< bookList.size(); i++){

            System.out.println(bookList.get(i).getBookState());

        }


    }*/

    @Ignore
    @Test
    public void testGetNumberOfSplit_검색해서_책이_몇권나왔는지_확인한다(){
        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();

        String html = "<div id=\"dataInfo0\" class=\"briefData first-child\"><div class=\"brief\"><span class=\"check\"><input type=\"checkbox\" name=\"data\" value=\"CAT000000502636\" class=\"checkbox\" title=\"checkbox\"></span><span class=\"linenum\">1.</span><span id=\"bookImg_CATTOT000000502636\" class=\"bookimg\"></span><dl class=\"briefDetail\"><dt class=\"skip\">서명</dt><dd class=\"searchTitle\"><a href=\"/search/detail/CATTOT000000502636?mainLink=/search/tot&amp;briefLink=/search/tot/result?os=DESC_A_cpp=10_A_q=%ED%95%B4%EB%A6%AC%ED%8F%AC%ED%84%B0%EC%99%80+%EB%B6%88%EC%9D%98+%EC%9E%94_A_st=KWRD_A_oi=DISP06_A_y=0_A_x=0_A_si=TOTAL_A_pn=1\"><span class=\"hilight\">해리포터와</span><span class=\"hilight\">불의</span><span class=\"hilight\">잔</span>[DVD] [DVD]</a>&nbsp;<img id=\"preview_open_CATTOT000000502636\" alt=\"미리보기\" title=\"미리보기\" src=\"/image/ko/common/ico/advance.gif\" onclick=\"preview('CATTOT000000502636')\" width=\"14\" height=\"13\" style=\"cursor: pointer;\"><img id=\"preview_close_CATTOT000000502636\" alt=\"미리보기닫기\" title=\"미리보기닫기\" src=\"/image/ko/common/ico/advance_on.gif\" onclick=\"preview('CATTOT000000502636')\" width=\"14\" height=\"13\" style=\"display:none;cursor: pointer;\"></dd><dt class=\"skip\">저자</dt><dd class=\"bookline\">뉴웰, 마이크</dd><dt class=\"skip\">출판사</dt><dd class=\"bookline\">워너브러더스 코리아</dd><dt class=\"skip\">청구기호</dt><dd class=\"bookline\">688.2 뉴67ㅎ</dd><dt class=\"skip\">발행년</dt><dd class=\"bookline\">2006</dd><dt class=\"skip\">자료유형</dt><dd class=\"bookline\"><img alt=\"시청각자료\" title=\"시청각자료\" src=\"/image/ko/common/ico/type_v.gif\" width=\"25\" height=\"20\">시청각자료</dd><dt class=\"skip\">소장처</dt><dd class=\"bookline\"><a href=\"#callLocation\" class=\"briefDeFont\" onkeydown=\"javascript:callLocation('0','1','CAT000000502636','F0000001')\" onmousedown=\"javascript:callLocation('0','1','CAT000000502636','F0000001')\">학산도서관<img id=\"locImg_CAT0000005026360\" alt=\"열기\" src=\"/image/ko/common/ico/down.gif\"></a></dd></dl></div><div id=\"prevData_CATTOT000000502636\" style=\"display:none;\" class=\"searchPreview\"></div><div id=\"prevLoc_CAT0000005026360\" style=\"display:none;\" class=\"possessview\"></div></div><div id=\"dataInfo1\" class=\"briefData \"><div class=\"brief\"><span class=\"check\"><input type=\"checkbox\" name=\"data\" value=\"CAT000000391874\" class=\"checkbox\" title=\"checkbox\"></span><span class=\"linenum\">2.</span><span id=\"bookImg_CATTOT000000391874\" class=\"bookimg\"></span><dl class=\"briefDetail\"><dt class=\"skip\">서명</dt><dd class=\"searchTitle\"><a href=\"/search/detail/CATTOT000000391874?mainLink=/search/tot&amp;briefLink=/search/tot/result?os=DESC_A_cpp=10_A_q=%ED%95%B4%EB%A6%AC%ED%8F%AC%ED%84%B0%EC%99%80+%EB%B6%88%EC%9D%98+%EC%9E%94_A_st=KWRD_A_oi=DISP06_A_y=0_A_x=0_A_si=TOTAL_A_pn=1\"><span class=\"hilight\">해리포터와</span><span class=\"hilight\">불의</span><span class=\"hilight\">잔</span>[비디오 녹화자료]</a>&nbsp;<img id=\"preview_open_CATTOT000000391874\" alt=\"미리보기\" title=\"미리보기\" src=\"/image/ko/common/ico/advance.gif\" onclick=\"preview('CATTOT000000391874')\" width=\"14\" height=\"13\" style=\"cursor: pointer;\"><img id=\"preview_close_CATTOT000000391874\" alt=\"미리보기닫기\" title=\"미리보기닫기\" src=\"/image/ko/common/ico/advance_on.gif\" onclick=\"preview('CATTOT000000391874')\" width=\"14\" height=\"13\" style=\"display:none;cursor: pointer;\"></dd><dt class=\"skip\">저자</dt><dd class=\"bookline\">Newell, Mike</dd><dt class=\"skip\">출판사</dt><dd class=\"bookline\">워너브러더스코리아 [수입·배급]</dd><dt class=\"skip\">청구기호</dt><dd class=\"bookline\">688.2 해239b</dd><dt class=\"skip\">발행년</dt><dd class=\"bookline\">2006</dd><dt class=\"skip\">자료유형</dt><dd class=\"bookline\"><img alt=\"시청각자료\" title=\"시청각자료\" src=\"/image/ko/common/ico/type_v.gif\" width=\"25\" height=\"20\">시청각자료</dd><dt class=\"skip\">소장처</dt><dd class=\"bookline\"><a href=\"#callLocation\" class=\"briefDeFont\" onkeydown=\"javascript:callLocation('0','1','CAT000000391874','F0000001')\" onmousedown=\"javascript:callLocation('0','1','CAT000000391874','F0000001')\">학산도서관<span class=\"book_state\">대출불가</span><img id=\"locImg_CAT0000003918740\" alt=\"열기\" src=\"/image/ko/common/ico/down.gif\"></a></dd></dl></div><div id=\"prevData_CATTOT000000391874\" style=\"display:none;\" class=\"searchPreview\"></div><div id=\"prevLoc_CAT0000003918740\" style=\"display:none;\" class=\"possessview\"></div></div><div id=\"dataInfo2\" class=\"briefData \"><div class=\"brief\"><span class=\"check\"><input type=\"checkbox\" name=\"data\" value=\"CAT000000174065\" class=\"checkbox\" title=\"checkbox\"></span><span class=\"linenum\">3.</span><span id=\"bookImg_CATTOT000000174065\" class=\"bookimg\"></span><dl class=\"briefDetail\"><dt class=\"skip\">서명</dt><dd class=\"searchTitle\"><a href=\"/search/detail/CATTOT000000174065?mainLink=/search/tot&amp;briefLink=/search/tot/result?os=DESC_A_cpp=10_A_q=%ED%95%B4%EB%A6%AC%ED%8F%AC%ED%84%B0%EC%99%80+%EB%B6%88%EC%9D%98+%EC%9E%94_A_st=KWRD_A_oi=DISP06_A_y=0_A_x=0_A_si=TOTAL_A_pn=1\"><span class=\"hilight\">해리포터와</span><span class=\"hilight\">불의</span><span class=\"hilight\">잔</span></a>&nbsp;<img id=\"preview_open_CATTOT000000174065\" alt=\"미리보기\" title=\"미리보기\" src=\"/image/ko/common/ico/advance.gif\" onclick=\"preview('CATTOT000000174065')\" width=\"14\" height=\"13\" style=\"cursor: pointer;\"><img id=\"preview_close_CATTOT000000174065\" alt=\"미리보기닫기\" title=\"미리보기닫기\" src=\"/image/ko/common/ico/advance_on.gif\" onclick=\"preview('CATTOT000000174065')\" width=\"14\" height=\"13\" style=\"display:none;cursor: pointer;\"></dd><dt class=\"skip\">저자</dt><dd class=\"bookline\">롤링, 조앤 K</dd><dt class=\"skip\">출판사</dt><dd class=\"bookline\">문학수첩</dd><dt class=\"skip\">청구기호</dt><dd class=\"bookline\">843 R884하</dd><dt class=\"skip\">발행년</dt><dd class=\"bookline\">2000</dd><dt class=\"skip\">자료유형</dt><dd class=\"bookline\"><img alt=\"단행본\" title=\"단행본\" src=\"/image/ko/common/ico/type_m.gif\" width=\"25\" height=\"20\">단행본</dd><dt class=\"skip\">소장처</dt><dd class=\"bookline\"><a href=\"#callLocation\" class=\"briefDeFont\" onkeydown=\"javascript:callLocation('0','1','CAT000000174065','F0000001')\" onmousedown=\"javascript:callLocation('0','1','CAT000000174065','F0000001')\">학산도서관<span class=\"book_state\">대출가능</span><img id=\"locImg_CAT0000001740650\" alt=\"열기\" src=\"/image/ko/common/ico/down.gif\"></a></dd></dl></div><div id=\"prevData_CATTOT000000174065\" style=\"display:none;\" class=\"searchPreview\"></div><div id=\"prevLoc_CAT0000001740650\" style=\"display:none;\" class=\"possessview\n";

        int number = 0;

        number = processingHtmlData.getNumberOfSplits(html);

        System.out.println(number);
    }

    @Ignore
    @Test
    public void testGetPageInfo_페이지_정보를_가져온다(){
        ParsingHtmlThread parsingHtmlThread = new ParsingHtmlThread();
        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();

        SitePage sitePage;

        String regex = "<strong>\\d+/\\d+</strong>";

        String basicUrl = "http://lib.inu.ac.kr/search/tot/result?q=%EC%9A%B4%EC%98%81%EC%B2%B4%EC%A0%9C&st=KWRD&si=TOTAL&oi=DISP06&os=DESC&cpp=10";

        String parsingResult = parsingHtmlThread.DownloadHtml(basicUrl);
        String pageInfo = processingHtmlData.splitArea(parsingResult, regex);

        sitePage = processingHtmlData.getPageInfo(pageInfo);

        System.out.println("현재 : " + sitePage.getBookCurrentPage() + " 전체 : " + sitePage.getBookTotalPage());
    }

    @Test
    public void testDistinguishLocation(){

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
        List<Book> locationList = new ArrayList<>();

        for(int i=0; i<10; i++){
            Book book = new Book();

            if(i<6)
                book.setBookLocation("사범대학교/자료실/");
            else if(i > 6 && i < 9)
                book.setBookLocation("학산도서관/제2자료실(4층)");
            else
                book.setBookLocation("동북아통상대학/자료실/");

            locationList.add(book);
        }

        for(Book book : locationList){
            System.out.println(book.getBookLocation());
        }

        Book resultBook = processingHtmlData.distinguishLocation(locationList);

        System.out.println(resultBook.getBookBorrowableHaksan());
        System.out.println(resultBook.getBookBorrowableEducation());
        System.out.println(resultBook.getBookBorrowableSonas());


    }

    @Test
    public void testGetCoverImage(){

        ParsingHtmlThread thread = new ParsingHtmlThread();

        String result = thread.DownloadHtml("http://lib.inu.ac.kr/search/detail/CATTOT000000778101?mainLink=/search/tot&briefLink=/search/tot/result?q=%EB%85%B8%EB%8F%99+%EC%97%86%EB%8A%94+%EB%AF%B8%EB%9E%98+_A_st=KWRD_A_y=0_A_x=0_A_si=TOTAL");

        System.out.println(result);

    }

}
