package com.inucseapp.inulibrary;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.inucseapp.inulibrary.activity.BookmarkActivity;
import com.inucseapp.inulibrary.activity.HistoryActivity;
import com.inucseapp.inulibrary.activity.ToolbarActivity;
import com.inucseapp.inulibrary.activity.V2_MainActivity;
import com.inucseapp.inulibrary.adapter.BookmarkAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.domain.AppVersion;
import com.inucseapp.inulibrary.tutorial.AppTutorial;

import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends ToolbarActivity {

    private static String TAG = "MainActivity";

    public static final String SEARCH_URL = "https://lib.inu.ac.kr/search/tot/result?";
    public static final String CONNECTION_CONFIRM_CLIENT_URL = "http://clients3.google.com/generate_204"; //네트워크 확인 url

    public static final int MSG_BEFORE_PARSING = 0 ;
    public static final int MSG_AFTER_PARSING = 1;
    public static final int MSG_PARSING_ERROR = 2;
    private Context context;

    private SQLHandler sqlHandler;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(Build.VERSION.SDK_INT>=21){
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        initializeView();

        if (isOnline()){
            //version check
            versionCheck();
        }
        else{
            new AlertDialog.Builder(this,R.style.AlertDialogCustom).setMessage("네트워크 연결을 확인 해주세요").setPositiveButton("종료", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    finish();
                }
            }).show();

        }

    }

    public void initializeView(){
        context = MainActivity.this;
        sqlHandler = new SQLHandler(context);
    }

    //version check
    private void versionCheck() {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference myRef = rootRef.getRoot();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                AppVersion data = dataSnapshot.child("version").getValue(AppVersion.class);
                //Log.i(TAG,dataSnapshot.child("version").toString());
                chkUpdateVersion(data);
            }
            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                //Log.e(TAG, "onCancelled: error");
            }
        });

    }

    private void chkUpdateVersion(AppVersion data) {
        //Log.i(TAG,"chkUpdateVersion");
        int appVersionCode = BuildConfig.VERSION_CODE;
        long latestVersionCode = data.getLastest_version_code();

        //Log.i(TAG, "appVersionCode : " + appVersionCode + ", latestVersionCode : " + latestVersionCode);

        if(appVersionCode < latestVersionCode){
            updatePopup(data.getMessage());
        } else {

            Intent intent = new Intent(getApplicationContext(),V2_MainActivity.class);
            startActivity(intent);
            finish();

/*
            if(sqlHandler.select_bookmarkInfo() == null ){
                //bookmark목록이 없으면 검색 페이지로 이동.
                Intent intent = new Intent(context, HistoryActivity.class);
                startActivity(intent);
                finish();

            }else{
                //bookmark목록이 있으면 북마크페이지로 이동.
                Intent intent = new Intent(context, BookmarkActivity.class);
                startActivity(intent);
                finish();
            }


            //tutorial
            if(sqlHandler.selectTutorial("idx") == null) {

                //App을 처음 동작시킴.
                sqlHandler.insertInitializeTutorial();

                Intent intent = new Intent(context, AppTutorial.class);
                startActivity(intent);

            }*/


        }
    }

    private void updatePopup(String message){
        new AlertDialog.Builder(this,R.style.AlertDialogCustom).setMessage(message).setPositiveButton("Install", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Log.i(TAG,"마켓이동");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + getPackageName()));
                startActivity(intent);
                finish();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                finish();
            }
        }).show();

    }

    public static boolean isOnline() {
        CheckConnect cc = new CheckConnect(CONNECTION_CONFIRM_CLIENT_URL);
        cc.start();
        try{
            cc.join();
            return cc.isSuccess();
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private static class CheckConnect extends Thread{
        private boolean success;
        private String host;

        public CheckConnect(String host){
            this.host = host;
        }

        @Override
        public void run() {

            HttpURLConnection conn = null;
            try {
                conn = (HttpURLConnection)new URL(host).openConnection();
                conn.setRequestProperty("User-Agent","Android");
                conn.setConnectTimeout(1000);
                conn.connect();
                int responseCode = conn.getResponseCode();
                if(responseCode == 204) success = true;
                else success = false;
            }
            catch (Exception e) {
                e.printStackTrace();
                success = false;
            }
            if(conn != null){
                conn.disconnect();
            }
        }

        public boolean isSuccess(){
            return success;
        }

    }
}
