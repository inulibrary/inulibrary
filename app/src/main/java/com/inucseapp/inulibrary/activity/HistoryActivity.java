package com.inucseapp.inulibrary.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.HistoryAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.domain.BookSearchCondition;
import com.inucseapp.inulibrary.domain.SearchHistory;
import com.inucseapp.inulibrary.tutorial.HistoryTutorial;

import java.util.List;

/**
 * Created by syg11 on 2017-07-06.
 */

public class HistoryActivity extends SearchToolbarActivity implements AdapterView.OnItemSelectedListener{

    private static String TAG = "HistoryActivity";

    private Context context;

    private Toolbar toolbar;
    private SQLHandler sqlHandler;

    private RecyclerView historyRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private HistoryAdapter historyAdapter;

    private Spinner typeSpinner;
    private Spinner orderSpinner;
    private SpinnerAdapter typeSpinnerAdapter;
    private SpinnerAdapter orderSpinnerAdapter;

    private BookSearchCondition bookSearchCondition;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //Log.i(TAG, "HistoryActivity");

        initializeView();

        if(sqlHandler.selectTutorial("history").get(0) == 0) {

            sqlHandler.updateTutorial("history");

            Intent intent = new Intent(this, HistoryTutorial.class);
            startActivity(intent);

        }

        this.loadHistoryInfo();

    }

    @Override
    protected void onResume() {

        //Log.i(TAG, "onResume()");

        this.initializeView();
        this.loadHistoryInfo();

        super.onResume();
    }

    public void initializeView(){

        context = HistoryActivity.this;

        //AD
        MobileAds.initialize(getApplicationContext(),"ca-app-pub-9972298440996794~1848779003");
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5308EA1293651F31192A565607B498E7").build();
        adView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sqlHandler = new SQLHandler(getApplicationContext());

        historyRecyclerView = (RecyclerView) findViewById(R.id.history_recyclerview);
        linearLayoutManager = new LinearLayoutManager((getApplicationContext()));
        historyRecyclerView.setHasFixedSize(true);
        historyRecyclerView.setLayoutManager(linearLayoutManager);

        typeSpinner = (Spinner) findViewById(R.id.search_type_spinner);
        typeSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.search, android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(this);

        orderSpinner = (Spinner) findViewById(R.id.order_spinner);
        orderSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.order, android.R.layout.simple_spinner_dropdown_item);
        orderSpinner.setAdapter(orderSpinnerAdapter);
        orderSpinner.setOnItemSelectedListener(this);

        //디비에 있는지 확인
        bookSearchCondition = sqlHandler.selectSearchCondition();

        if(bookSearchCondition != null){
            //있으면 가져와서 setSelection에 설정
            //Log.i(TAG, String.valueOf(this.convertTypeSpinnerToIndex(bookSearchCondition.getSortMethod())));
            typeSpinner.setSelection(this.convertTypeSpinnerToIndex(bookSearchCondition.getSortMethod()));
            orderSpinner.setSelection(this.convertOrderSpinnerToIndex(bookSearchCondition.getSort()));

        }else{
            //없으면 새로 넣음.
            //Log.i(TAG, "bookSearchCondition is null");
            sqlHandler.insertInitializeSearchCondition();
            typeSpinner.setSelection(0);
            orderSpinner.setSelection(0);
        }

    }

    public void loadHistoryInfo(){

        List<SearchHistory> searchHistoryList = sqlHandler.selectHistory();

        historyAdapter = new HistoryAdapter(context, searchHistoryList);
        historyRecyclerView.setAdapter(historyAdapter);

    }

    public int convertTypeSpinnerToIndex(String option){

        switch(option){
            case "서명" :
                return 0;

            case "저자" :
                return 1;

            case "출판사" :
                return 2;

            case "청구기호" :
                return 3;

            case "발행년" :
                return 4;

            default :
                return 0;
        }

    }

    public int convertOrderSpinnerToIndex(String option){
        switch(option){
            case "오름차순" :
                return 0;

            case "내림차순" :
                return 1;

            default :
                return 0;
        }
    }

    //한글 분류를 DISP01 꼴로 변경해줄 부분이 필요.
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner) parent;

        switch(spinner.getId()){

            case R.id.search_type_spinner :

                sqlHandler.updateTypeCondition(String.valueOf(typeSpinnerAdapter.getItem(position)));


                break;

            case R.id.order_spinner :

                sqlHandler.updateOrderCondition(String.valueOf(orderSpinnerAdapter.getItem(position)));
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
