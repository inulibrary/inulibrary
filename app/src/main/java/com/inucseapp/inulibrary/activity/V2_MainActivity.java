package com.inucseapp.inulibrary.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.inucseapp.inulibrary.R;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class V2_MainActivity extends AppCompatActivity {
    BottomBar bottomBar;

    Fragment fragment1;
    Fragment fragment2;
    Fragment fragment3;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_activity_main);

        bottomBar = (BottomBar) findViewById(R.id.bottom_bar);

        fragment1 = new V2_FavoritesFragment();
        fragment2 = new V2_SearchFragment();
        fragment3 = new V2_MenuFragment();

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(int tabId) {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                switch (tabId){
                    case R.id.tab_favorites:
                        fragmentTransaction.replace(R.id.container,fragment1);
                        break;
                    case R.id.tab_search:
                        fragmentTransaction.replace(R.id.container,fragment2);
                        break;
                    case R.id.tab_menu:
                        fragmentTransaction.replace(R.id.container,fragment3);
                        break;
                }
                fragmentTransaction.commit();
            }
        });
    }
}
