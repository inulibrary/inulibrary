package com.inucseapp.inulibrary.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inucseapp.inulibrary.R;

public class V2_MenuFragment extends Fragment {

    private View rootView;

    private TextView historyOption;
    private TextView favoriteOption;
    private TextView developerOption;
    private TextView applicationReviewOption;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.v2_fragment_menu,container,false);

        initialize(rootView);
        return rootView;
    }

    private void initialize(View v) {
        historyOption = (TextView) v.findViewById(R.id.history_option_item);
        favoriteOption = (TextView) v.findViewById(R.id.favorite_option_item);
        developerOption = (TextView) v.findViewById(R.id.developer_info_item);
        applicationReviewOption = (TextView)v.findViewById(R.id.application_review_item);


        historyOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(rootView.getContext(),V2_DeleteHistoryActivity.class);
                startActivity(intent);
            }
        });

        favoriteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(rootView.getContext(),V2_DeleteFavoritesActivity.class);
                startActivity(intent);
            }
        });

        developerOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(rootView.getContext(),DeveloperActivity.class);
                startActivity(intent);
            }
        });

        applicationReviewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.inucse.inulibrary"));
                startActivity(intent);
            }
        });
    }
}
