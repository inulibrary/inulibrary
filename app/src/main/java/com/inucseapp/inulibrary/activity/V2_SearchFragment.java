package com.inucseapp.inulibrary.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.HistoryAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.domain.BookSearchCondition;
import com.inucseapp.inulibrary.domain.SearchHistory;

import java.util.List;

public class V2_SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    View rootView;

    private SQLHandler sqlHandler;

    private RecyclerView historyRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private HistoryAdapter historyAdapter;

    private Spinner typeSpinner;
    private Spinner orderSpinner;
    private SpinnerAdapter typeSpinnerAdapter;
    private SpinnerAdapter orderSpinnerAdapter;

    EditText searchText;

    private BookSearchCondition bookSearchCondition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.v2_fragment_search,container,false);

        initialize(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadHistoryInfo(rootView);
    }

    private void initialize(View v) {
        sqlHandler = new SQLHandler(v.getContext());

        searchText = (EditText) v.findViewById(R.id.search_text);
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                String query = textView.getText().toString();
                switch (i){
                    case EditorInfo.IME_ACTION_SEARCH:
                        SQLHandler sqlHandler = new SQLHandler(rootView.getContext());

                        if(sqlHandler.selectHistoryTitle(query) == null){
                            sqlHandler.insertHistory(query);
                        }else{
                            sqlHandler.updateHistory(query);
                        }

                        Intent intent = new Intent(rootView.getContext(), SearchResultActivity.class);
                        intent.putExtra("query", query);

                        startActivity(intent);
                        break;
                }
                return false;
            }
        });

        historyRecyclerView = (RecyclerView) v.findViewById(R.id.history_recyclerview);
        linearLayoutManager = new LinearLayoutManager(v.getContext());
        historyRecyclerView.setHasFixedSize(true);
        historyRecyclerView.setLayoutManager(linearLayoutManager);

        typeSpinner = (Spinner) v.findViewById(R.id.search_type_spinner);
        typeSpinnerAdapter = ArrayAdapter.createFromResource(v.getContext(), R.array.search, android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setOnItemSelectedListener(this);

        orderSpinner = (Spinner) v.findViewById(R.id.order_spinner);
        orderSpinnerAdapter = ArrayAdapter.createFromResource(v.getContext(), R.array.order, android.R.layout.simple_spinner_dropdown_item);
        orderSpinner.setAdapter(orderSpinnerAdapter);
        orderSpinner.setOnItemSelectedListener(this);

        //디비에 있는지 확인
        bookSearchCondition = sqlHandler.selectSearchCondition();

        if(bookSearchCondition != null){
            //있으면 가져와서 setSelection에 설정
            //Log.i(TAG, String.valueOf(this.convertTypeSpinnerToIndex(bookSearchCondition.getSortMethod())));
            typeSpinner.setSelection(this.convertTypeSpinnerToIndex(bookSearchCondition.getSortMethod()));
            orderSpinner.setSelection(this.convertOrderSpinnerToIndex(bookSearchCondition.getSort()));

        }else{
            //없으면 새로 넣음.
            //Log.i(TAG, "bookSearchCondition is null");
            sqlHandler.insertInitializeSearchCondition();
            typeSpinner.setSelection(0);
            orderSpinner.setSelection(0);
        }
    }

    public void loadHistoryInfo(View v){

        List<SearchHistory> searchHistoryList = sqlHandler.selectHistory();

        historyAdapter = new HistoryAdapter(v.getContext(), searchHistoryList);
        historyAdapter.setFragmentView(rootView);
        historyRecyclerView.setAdapter(historyAdapter);

    }

    public int convertTypeSpinnerToIndex(String option){

        switch(option){
            case "서명" :
                return 0;

            case "저자" :
                return 1;

            case "출판사" :
                return 2;

            case "청구기호" :
                return 3;

            case "발행년" :
                return 4;

            default :
                return 0;
        }

    }

    public int convertOrderSpinnerToIndex(String option){
        switch(option){
            case "오름차순" :
                return 0;

            case "내림차순" :
                return 1;

            default :
                return 0;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        Spinner spinner = (Spinner) parent;

        switch(spinner.getId()){

            case R.id.search_type_spinner :

                sqlHandler.updateTypeCondition(String.valueOf(typeSpinnerAdapter.getItem(position)));


                break;

            case R.id.order_spinner :

                sqlHandler.updateOrderCondition(String.valueOf(orderSpinnerAdapter.getItem(position)));
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
