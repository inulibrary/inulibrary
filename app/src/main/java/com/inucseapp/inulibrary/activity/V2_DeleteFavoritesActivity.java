package com.inucseapp.inulibrary.activity;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.BookmarkAdapter;
import com.inucseapp.inulibrary.adapter.DeleteFavoritesAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.dataprocessing.ProcessingHtmlData;
import com.inucseapp.inulibrary.domain.Book;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.inucseapp.inulibrary.MainActivity.MSG_AFTER_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_BEFORE_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_PARSING_ERROR;

public class V2_DeleteFavoritesActivity extends AppCompatActivity {
    final static String TAG = "DeleteFavoritesActivity";

    private static final String REGEX = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><p><\\/p>";


    private SQLHandler sqlHandler;

    private RecyclerView bookmarkRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SweetAlertDialog sweetAlertDialog;

    DeleteFavoritesAdapter deleteFavoritesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_activity_delete_favorites);

        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFavoritesInfo();
    }

    private void initialize() {

        sqlHandler = new SQLHandler(getApplicationContext());

        //RecyclerView
        bookmarkRecyclerView = (RecyclerView) findViewById(R.id.delete_favorites_recyclerview);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        bookmarkRecyclerView.setHasFixedSize(true);
        bookmarkRecyclerView.setLayoutManager(linearLayoutManager);

    }

    public void loadFavoritesInfo(){

        List<Book> bookList = sqlHandler.select_bookmarkInfo();

        deleteFavoritesAdapter = new DeleteFavoritesAdapter(getApplicationContext(), bookList, handler);
        bookmarkRecyclerView.setAdapter(deleteFavoritesAdapter);

    }

    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {

            switch(msg.what){
                case MSG_BEFORE_PARSING:
                    //Log.i(TAG, "parsing 전");

                    sweetAlertDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setTitleText("데이터 불러오는 중...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();

                    break;

                case MSG_AFTER_PARSING:
                    //Log.i(TAG, "parsing 후");

                    String parsingResult = msg.obj.toString();
                    ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
                    String splitArea = processingHtmlData.splitArea(parsingResult, REGEX);

                    List<String> detailHtmlList = processingHtmlData.getBookDetailInfo(splitArea);
                    List<Book> bookDetailInfoList = new ArrayList<>();

                    if(detailHtmlList != null){
                        for(int i=0; i<detailHtmlList.size(); i++){
                            Book book = processingHtmlData.getBookInfoFromDetailPage(detailHtmlList.get(i));
                            bookDetailInfoList.add(book);
                        }
                    }else{
                        sweetAlertDialog.dismiss();
                        break;
                    }

                    //새로 정보를 구성 책 등록번호, 총권수, 대여가능권수, 최근검색시간.
                    DateFormat dateFormat = new DateFormat();

                    //위치정보
                    Book locationInfo = processingHtmlData.distinguishLocation(bookDetailInfoList);

                    //새로 입력 객체
                    Book book = new Book();
                    book.setBookRegisterID(bookDetailInfoList.get(0).getBookRegisterID());
                    book.setBookTotalNum(String.valueOf(bookDetailInfoList.size()));
                    book.setBookBorrowableHaksan(locationInfo.getBookBorrowableHaksan());
                    book.setBookBorrowableEducation(locationInfo.getBookBorrowableEducation());
                    book.setBookBorrowableSonas(locationInfo.getBookBorrowableSonas());
                    book.setSearchDate(dateFormat.getCurrentTime());

                    sqlHandler.updateBookmarkInfo(book);

                    List<Book> bookList = sqlHandler.select_bookmarkInfo();

                    deleteFavoritesAdapter = new DeleteFavoritesAdapter(getApplicationContext(), bookList, handler);
                    bookmarkRecyclerView.setAdapter(deleteFavoritesAdapter);

                    sweetAlertDialog.dismiss();

                    break;

                case MSG_PARSING_ERROR:
                    Log.i(TAG, "parsing ERROR");
                    break;
            }

        }
    };

}
