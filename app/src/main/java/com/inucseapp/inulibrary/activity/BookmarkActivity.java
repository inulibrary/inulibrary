package com.inucseapp.inulibrary.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.BookmarkAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.dataprocessing.ProcessingHtmlData;
import com.inucseapp.inulibrary.domain.Book;
import com.inucseapp.inulibrary.tutorial.BookmarkTutorial;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.inucseapp.inulibrary.MainActivity.MSG_AFTER_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_BEFORE_PARSING;
import static com.inucseapp.inulibrary.MainActivity.MSG_PARSING_ERROR;

/**
 * Created by junha on 2017. 7. 29..
 */

public class BookmarkActivity extends ToolbarActivity{

    private static String TAG = "BookmarkActivity";

    public static final String SEARCH_URL = "http://lib.inu.ac.kr/search/tot/result?";

    private static final String REGEX = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><p><\\/p>";


    private Context context;
    private Toolbar toolbar;
    private RecyclerView bookmarkRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SweetAlertDialog sweetAlertDialog;

    private SQLHandler sqlHandler;

    private BookmarkAdapter bookmarkAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);

        initializeView();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("관심도서 목록");

        //처음 실행하면 tutorial로 넘어감.
        if(sqlHandler.selectTutorial("bookmark").get(0) == 0){
            sqlHandler.updateTutorial("bookmark");

            Intent intent = new Intent(context, BookmarkTutorial.class);
            startActivity(intent);
        }

        List<Book> bookList = sqlHandler.select_bookmarkInfo();

        bookmarkAdapter = new BookmarkAdapter(context, bookList, handler);
        bookmarkRecyclerView.setAdapter(bookmarkAdapter);


    }

    public void initializeView(){

        context = BookmarkActivity.this;

        //AD

        //AD
        MobileAds.initialize(getApplicationContext(),"ca-app-pub-9972298440996794~1848779003");
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("5308EA1293651F31192A565607B498E7").build();
        adView.loadAd(adRequest);

        //toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //RecyclerView
        bookmarkRecyclerView = (RecyclerView) findViewById(R.id.bookmark_recyclerview);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        bookmarkRecyclerView.setHasFixedSize(true);
        bookmarkRecyclerView.setLayoutManager(linearLayoutManager);

        sqlHandler = new SQLHandler(context);

    }

    public void loadBookmarkInfo(){

        List<Book> bookList = sqlHandler.select_bookmarkInfo();

        bookmarkAdapter = new BookmarkAdapter(getApplicationContext(), bookList, handler);
        bookmarkRecyclerView.setAdapter(bookmarkAdapter);

    }

    @Override
    protected void onResume() {

        this.loadBookmarkInfo();

        List<Book> bookList = sqlHandler.select_bookmarkInfo();

        bookmarkAdapter = new BookmarkAdapter(this.context, bookList, handler);
        bookmarkRecyclerView.setAdapter(bookmarkAdapter);

        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){

            case R.id.toolbar_tutorial:

                //Log.i(TAG, "toolbar_info");

                Intent intent = new Intent(context, BookmarkTutorial.class);
                startActivity(intent);

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {

            switch(msg.what){
                case MSG_BEFORE_PARSING:
                    //Log.i(TAG, "parsing 전");

                    sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setTitleText("데이터 불러오는 중...");
                    sweetAlertDialog.setCancelable(false);
                    sweetAlertDialog.show();

                    break;

                case MSG_AFTER_PARSING:
                    //Log.i(TAG, "parsing 후");

                    String parsingResult = msg.obj.toString();
                    ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();
                    String splitArea = processingHtmlData.splitArea(parsingResult, REGEX);

                    List<String> detailHtmlList = processingHtmlData.getBookDetailInfo(splitArea);
                    List<Book> bookDetailInfoList = new ArrayList<>();

                    if(detailHtmlList != null){
                        for(int i=0; i<detailHtmlList.size(); i++){
                            Book book = processingHtmlData.getBookInfoFromDetailPage(detailHtmlList.get(i));
                            bookDetailInfoList.add(book);
                        }
                    }else{
                        sweetAlertDialog.dismiss();
                        break;
                    }

                    //새로 정보를 구성 책 등록번호, 총권수, 대여가능권수, 최근검색시간.
                    DateFormat dateFormat = new DateFormat();

                    //위치정보
                    Book locationInfo = processingHtmlData.distinguishLocation(bookDetailInfoList);

                    //새로 입력 객체
                    Book book = new Book();
                    book.setBookRegisterID(bookDetailInfoList.get(0).getBookRegisterID());
                    book.setBookTotalNum(String.valueOf(bookDetailInfoList.size()));
                    book.setBookBorrowableHaksan(locationInfo.getBookBorrowableHaksan());
                    book.setBookBorrowableEducation(locationInfo.getBookBorrowableEducation());
                    book.setBookBorrowableSonas(locationInfo.getBookBorrowableSonas());
                    book.setSearchDate(dateFormat.getCurrentTime());

                    sqlHandler.updateBookmarkInfo(book);

                    List<Book> bookList = sqlHandler.select_bookmarkInfo();

                    bookmarkAdapter = new BookmarkAdapter(getApplicationContext(), bookList, handler);
                    bookmarkRecyclerView.setAdapter(bookmarkAdapter);

                    sweetAlertDialog.dismiss();

                    break;

                case MSG_PARSING_ERROR:
                    Log.i(TAG, "parsing ERROR");
                    break;
            }

        }
    };


}
