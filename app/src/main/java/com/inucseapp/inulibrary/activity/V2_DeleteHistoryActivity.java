package com.inucseapp.inulibrary.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.adapter.DeleteHistoryAdapter;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.domain.SearchHistory;

import java.util.List;

public class V2_DeleteHistoryActivity extends AppCompatActivity {
    private SQLHandler sqlHandler;

    private RecyclerView historyRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private DeleteHistoryAdapter deleteHistoryAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.v2_activity_delete_history);

        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadHistoryInfo();
    }

    private void initialize() {
        sqlHandler = new SQLHandler(getApplicationContext());

        historyRecyclerView = (RecyclerView) findViewById(R.id.delete_history_recyclerview);
        linearLayoutManager = new LinearLayoutManager((getApplicationContext()));
        historyRecyclerView.setHasFixedSize(true);
        historyRecyclerView.setLayoutManager(linearLayoutManager);
    }

    public void loadHistoryInfo(){

        List<SearchHistory> searchHistoryList = sqlHandler.selectHistory();

        deleteHistoryAdapter = new DeleteHistoryAdapter(getApplicationContext(), searchHistoryList);
        historyRecyclerView.setAdapter(deleteHistoryAdapter);

    }
}
