package com.inucseapp.inulibrary.domain;

/**
 * Created by junha on 2017. 2. 26..
 */

public class BookSearchCondition {
    private String keyword;         //q=
    private String searchType;      //st=KWRD
    private String y;               //y=0
    private String searchArrange;   //si=TOTAL
    private String x;               //x=0
    private String sortMethod;       //oi=DISP06
    private String sort;            //os=DESC
    private String resultNum;       //cpp=10
    private String pageNumber;      //pn=1,2,3,4,5.....

    //기본설정
    public BookSearchCondition(){

        this.setPageNumber("1");
        this.setSearchType("KWRD");
        this.setY("0");
        this.setSearchArrange("TOTAL");
        this.setX("0");
        this.setSortMethod("DISP06");
        this.setSort("DESC");
        this.setResultNum("10");

    }


    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword.replaceAll(" ","+");
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getSearchArrange() {
        return searchArrange;
    }

    public void setSearchArrange(String searchArrange) {
        this.searchArrange = searchArrange;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getSortMethod() {
        return sortMethod;
    }

    public void setSortMethod(String sortMethod) {
        this.sortMethod = sortMethod;
    }

    public String getSort() {
        return sort ;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getResultNum() {
        return resultNum;
    }

    public void setResultNum(String resultNum) {
        this.resultNum = resultNum;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    @Override
    public String toString (){
        return  "pn="   +getPageNumber()    + "&" +
                "q="    +getKeyword()       + "&" +    //여기 + 는 공백같은 느
                "st="   +getSearchType()    + "&" +
                "y="    + getY()            + "&" +
                "si="   + getSearchArrange()+ "&" +
                "x="    + getX()            + "&" +
                "oi="   + getSortMethod()   + "&" +
                "os="   + getSort() + "&"  +
                "cpp="  + getResultNum();
    }
}
