package com.inucseapp.inulibrary.dataprocessing;

import com.inucseapp.inulibrary.domain.Book;
import com.inucseapp.inulibrary.domain.SitePage;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by junha on 2017. 2. 27..
 */

public class ProcessingHtmlData {

    public String splitArea(String html, String regex){

        String pattern = String.format(regex);

        Pattern areaPattern = Pattern.compile(pattern);
        Matcher areaPatternMatches = areaPattern.matcher(html);

        String splitResult = "";

        while(areaPatternMatches.find()){
            splitResult = areaPatternMatches.group(0).trim();
        }

        return splitResult;
    }

    public int getNumberOfSplits(String html){

        String regex="<div id=\"dataInfo\\d?\\d?\\d";
        int numberOfSplits = 0;

        String pattern = String.format(regex);

        Pattern areaPattern = Pattern.compile(pattern);
        Matcher areaPatternMatches = areaPattern.matcher(html);

        while(areaPatternMatches.find()){
            numberOfSplits++;
        }

        return numberOfSplits;

    }

    public List<String> getBookInfoArea(String html){

        List<String> bookInfoResult = new ArrayList<>();
        int numberOfSplits = this.getNumberOfSplits(html);

        for(int i=0; i<numberOfSplits; i++){

            String regex= "<div id=\"dataInfo\\d?\\d?" + i + "\".*<div id=\"dataInfo\\d?\\d?" + (i+1) + "\"";

            if(i == numberOfSplits-1 ){
                //regex = "<div id=\"dataInfo" + i + "\".*<div id=\"divPaging\"";

                regex = "<div id=\"dataInfo\\d?\\d?" + i + "\".*<dt class=\"skip\">\\s*자료유형";
            }

            String bookInfo = this.splitArea(html, regex);
            bookInfoResult.add(bookInfo);
        }

        return bookInfoResult;

    }

    public Book getBookInfoFromSearchPage(String bookHtml){

        Book book = new Book();

        String html = bookHtml.replaceAll("<span class=\"hilight\">","</span>").replaceAll("</span>","");

        String title;
        String author;
        String publisher;
        String bookId;
        String year;
        String bookUrl;


        String temp = this.splitArea(html, "<dt\\sclass=\"skip\">서명<\\/dt>.+<\\/a><img");
        //System.out.println("temp : " + temp);

        title = this.splitArea(temp,"briefLink.*<\\/a><img")
                        .replaceAll(".+\">","")
                        .replaceAll("</a><img","");
        title = StringEscapeUtils.unescapeHtml4(title);

        bookUrl = this.splitArea(temp,"<a\\shref=.+\">")
                        .replaceAll("<a\\shref=\"\\/search\\/detail\\/","")
                        .replaceAll("\\?mainLink.+\">","");

        author = this.splitArea(html,"저자<\\/dt><dd class=\"bookline\">.+<\\/dd><dt class=\"skip\">출판사")
                        .replaceAll("저자<\\/dt><dd class=\"bookline\">","")
                        .replaceAll("<\\/dd><dt\\sclass=\"skip\">출판사","");

        //출판사</dt><dd class="bookline">다른</dd><dt class="skip">청구기호
        publisher = this.splitArea(html,"출판사</dt><dd class=\"bookline\">.+</dd><dt class=\"skip\">청구기호")
                        .replaceAll("출판사<\\/dt><dd class=\"bookline\">","")
                        .replaceAll("</dd><dt\\sclass=\"skip\">청구기호","");

        //청구기호</dt><dd class="bookline">802.3 K69c정</dd><dt class="skip">발행년
        bookId = this.splitArea(html,"청구기호</dt><dd class=\"bookline\">.+</dd><dt class=\"skip\">발행년")
                        .replaceAll("청구기호<\\/dt><dd class=\"bookline\">","")
                        .replaceAll("</dd><dt class=\"skip\">발행년","");

        year = this.splitArea(html,"발행년</dt><dd class=\"bookline\">.+</dd><dt class=\"skip\">자료유형")
                        .replaceAll("발행년</dt><dd class=\"bookline\">","")
                        .replaceAll("</dd><dt class=\"skip\">자료유형","");

        book.setBookTitle(title);
        book.setAuthor(author);
        book.setPublisher(publisher);
        book.setBookID(bookId);
        book.setIssueYear(year);
        book.setBookUrl(bookUrl);

        return book;
    }

    public List<String> getBookDetailInfo (String html){

        ProcessingHtmlData processingHtmlData = new ProcessingHtmlData();

        int index=1;
        String regexBasic;
        String regexLastBook;
        String bookInfoArea;

        List<String> bookDetailList = new ArrayList<>();

        while(true){

            regexBasic = "<td>"+ (index) +"<\\/td>.*<td>"+ (index+1) +"<\\/td>";
            regexLastBook = "<td>"+ (index) +"<\\/td>.*<td><div style=\"display";

            bookInfoArea = processingHtmlData.splitArea(html, regexBasic);

            if("".equals(bookInfoArea)){
                //자료가 하나 이거나 마지막 index자료인 경우
                bookInfoArea = processingHtmlData.splitArea(html, regexLastBook);

                if("".equals(bookInfoArea)){
                    //이거로 검색해도 자료가 없는 경우
                    return null;
                }else {
                    bookDetailList.add(bookInfoArea);
                }

                return bookDetailList;
            }

            bookDetailList.add(bookInfoArea);

            index++;
        }
    }

    public Book getBookInfoFromDetailPage (String detailInfo){

        Book book = new Book();

        detailInfo = detailInfo.replaceAll("<\\/td><td><\\/td><td><a class=\"service\".*", "")
                .replaceAll("<td>","")
                .replaceAll("<td class=\"point\">","");

        String[] spl = detailInfo.split("</td>");

        book.setIndex(spl[0]);                    //index -> 필요없음
        book.setBookRegisterID(spl[1]);           //도서등록번호
        book.setBookID(spl[2]);                   //bookID
        book.setBookLocation(spl[3].replaceAll("\\)/", "\\)").replaceAll("실/","실"));             //소장처
        book.setBookState(spl[4]);                //도서상태

        if(spl.length > 5) {
            book.setBookReturnDate(spl[5]);           //반납예정일
        }

       return book;
    }

    //현재 페이지 정보
    public SitePage getPageInfo (String pageInfo){

        SitePage sitePage = new SitePage();

        String[] spl = pageInfo.replaceAll("</?strong>", "").split("/");

        sitePage.setBookCurrentPage(Integer.parseInt(spl[0]));
        sitePage.setBookTotalPage(Integer.parseInt(spl[1]));

        return sitePage;

    }

    public String getBorrowAvailNumber(List<Book> bookList){

        int num = 0;

        for(int i=0; i<bookList.size(); i++){
            if("대출가능".equals(bookList.get(i).getBookState())){
                num++;
            }
        }

        return String.valueOf(num);
    }

    public Book distinguishLocation(List<Book> bookList){

        Book borrowableBooks = new Book();
        int haksan = 0;
        int education = 0;
        int sonas = 0;

        for(Book book : bookList){

            if("대출가능".equals(book.getBookState())) {
                if (book.getBookLocation().contains("학산")) {
                    haksan++;
                } else if (book.getBookLocation().contains("사범")) {
                    education++;
                } else if (book.getBookLocation().contains("동북아")) {
                    sonas++;
                }
            }
        }

        borrowableBooks.setBookBorrowableHaksan(String.valueOf(haksan));
        borrowableBooks.setBookBorrowableEducation(String.valueOf(education));
        borrowableBooks.setBookBorrowableSonas(String.valueOf(sonas));

        return borrowableBooks;
    }

    public String getCoverImage(String html){

        String regex = "";


        return null;
    }

}
