package com.inucseapp.inulibrary.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.domain.Book;
import com.inucseapp.inulibrary.domain.BookSearchCondition;
import com.inucseapp.inulibrary.domain.SearchHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by junha on 2017. 7. 24..
 */

public class SQLHandler {

    private SqliteOpenHelper helper;
    String dbName = "library.db";
    int dbVersion = 1;
    private SQLiteDatabase db;
    String TAG = "SQLite";

    Context context = null;

    public SQLHandler(Context context){
        this.context = context;
        helper = new SqliteOpenHelper(context, dbName, null, dbVersion);
    }

    public void setWritableDB(){
        db = helper.getWritableDatabase();
    }

    public void setReadableDB(){
        db = helper.getReadableDatabase();
    }

    public void insert(){
        this.setWritableDB();

        ContentValues values = new ContentValues();

        values.put("title", "테스트");

        db.insert("bookinfo", null, values);
    }

    public void insert_bookinfo(Book book){
        this.setWritableDB();

        ContentValues values = new ContentValues();

        values.put("title", book.getBookTitle());
        values.put("author", book.getAuthor());
        values.put("publisher", book.getPublisher());
        values.put("id", book.getBookID());
        values.put("url", book.getBookUrl());
        values.put("issue", book.getIssueYear());
        values.put("registerid", book.getBookRegisterID());
        values.put("haksan", book.getBookBorrowableHaksan());
        values.put("education", book.getBookBorrowableEducation());
        values.put("sonas", book.getBookBorrowableSonas());
        values.put("totalbooks", book.getBookTotalNum());
        values.put("bmregister", book.getSearchDate());

        db.insert("bookinfo", null, values);

        Log.i(TAG, book.getSearchDate());
    }

    public void selectAll(){
        this.setReadableDB();

        Cursor c = db.query("bookinfo", null, null, null, null, null, null);

        while(c.moveToNext()){
            int index = c.getInt(c.getColumnIndex("idx"));
            String bookTitle = c.getString(c.getColumnIndex("title"));
            String bookAuthor = c.getString(c.getColumnIndex("author"));
            String bookPublisher = c.getString(c.getColumnIndex("publisher"));
            String bookId = c.getString(c.getColumnIndex("id"));
            String bookURL = c.getString(c.getColumnIndex("url"));
            String bookRegisterID = c.getString(c.getColumnIndex("registerid"));
            String bookHaksan = c.getString(c.getColumnIndex("haksan"));
            String bookEducation = c.getString(c.getColumnIndex("education"));
            String bookSonas = c.getString(c.getColumnIndex("sonas"));
            String bookTotalBooks = c.getString(c.getColumnIndex("totalbooks"));
            String bmregister = c.getString(c.getColumnIndex("bmregister"));

            Log.i(TAG, index + ", " + bookTitle + ", " + bookAuthor + ", " + bookPublisher + ", " + bookRegisterID + ", "
                    + bookId + ", " + bookURL + ", " + bookHaksan + ", " + bookEducation + ", " + bookSonas + ", " + bookTotalBooks +  ", " + bmregister);
        }

        c.close();
    }

    //desc 검색일.
    public List<Book> select_bookmarkInfo(){

        List<Book> bookList = new ArrayList<>();

        this.setReadableDB();

        Cursor c = db.rawQuery("select * from bookinfo order by idx desc;", null);
        //Cursor c = db.query("bookinfo", null, null, null, null, null, null);

        while(c.moveToNext()){

            Book book = new Book();

            book.setBookTitle(c.getString(c.getColumnIndex("title")));
            book.setAuthor(c.getString(c.getColumnIndex("author")));
            book.setPublisher(c.getString(c.getColumnIndex("publisher")));
            book.setBookID(c.getString(c.getColumnIndex("id")));
            book.setBookUrl(c.getString(c.getColumnIndex("url")));
            book.setBookRegisterID(c.getString(c.getColumnIndex("registerid")));
            book.setBookTotalNum(c.getString(c.getColumnIndex("totalbooks")));
            book.setBookBorrowableHaksan(c.getString(c.getColumnIndex("haksan")));
            book.setBookBorrowableEducation(c.getString(c.getColumnIndex("education")));
            book.setBookBorrowableSonas(c.getString(c.getColumnIndex("sonas")));
            book.setSearchDate(c.getString(c.getColumnIndex("bmregister")));
            book.setIssueYear(c.getString(c.getColumnIndex("issue")));

            bookList.add(book);
        }

        if(bookList.size() != 0){
            return bookList;
        }else {
            return null;
        }

    }

    public boolean select_bookid(String bookID){

        List<String> bookIdList = new ArrayList<>();

        this.setReadableDB();

        Cursor c = db.rawQuery("select id from bookinfo;", null);

        while(c.moveToNext()) {
            bookIdList.add(c.getString(c.getColumnIndex("id")));
        }

        if(bookIdList.contains(bookID)){
            return true;
        }else{
            return false;
        }
    }

    public void updateBookmarkInfo(Book book){

        this.setWritableDB();

//        Log.i(TAG, "updateBookmarkInfo()");

        db.execSQL("update bookinfo set totalbooks='" + book.getBookTotalNum() +
                        "', haksan='" + book.getBookBorrowableHaksan() +
                        "', education='" + book.getBookBorrowableEducation() +
                        "', sonas='" + book.getBookBorrowableSonas() +
                        "', bmregister='" + book.getSearchDate() +
                        "' where registerid = '" + book.getBookRegisterID() +
                        "';");

//        Log.d(TAG, "bookmark update 완료");

    }

    public void deleteBookmarkInfo(Book book){
        this.setWritableDB();

//        Log.i(TAG, "deleteBookmarkInfo()");

        db.execSQL("delete from bookinfo where registerid='" + book.getBookRegisterID() + "';");
    }



    //SearchHsitory

    public List<SearchHistory> selectHistory(){
        this.setReadableDB();

        List<SearchHistory> searchHistoryList = new ArrayList<>();

        Cursor c = db.rawQuery("select * from searchhistory order by date desc;", null);

        while(c.moveToNext()){

            SearchHistory searchHistory = new SearchHistory();

            searchHistory.setIndex(c.getInt(c.getColumnIndex("idx")));
            searchHistory.setSearchWord(c.getString(c.getColumnIndex("word")));
            searchHistory.setSearchDate(c.getString(c.getColumnIndex("date")));

            searchHistoryList.add(searchHistory);

        }

        return searchHistoryList;
    }

    public String selectHistoryTitle(String query){
        this.setReadableDB();

        String result = null;

        Cursor c = db.rawQuery("select word from searchhistory where word='" + query + "';", null);

        while(c.moveToNext()){

            result = c.getString(c.getColumnIndex("word"));
//            Log.i(TAG, "historyTitle Result : " + result);

        }

        return result;


    }

    public void insertHistory(String query){

        this.setWritableDB();

        ContentValues values = new ContentValues();

        //date
        DateFormat dateFormat = new DateFormat();

        values.put("word", query);
        values.put("date", dateFormat.getCurrentTime());

//        Log.i(TAG, "query : " + query + ", date : " + dateFormat.convertDateForamt(dateFormat.getCurrentTime()));

        db.insert("searchhistory", null, values);
    }

    public void deleteHistory(SearchHistory searchHistory){
        this.setWritableDB();

        db.execSQL("delete from searchhistory where date='" + searchHistory.getSearchDate() + "';");
    }

    public void updateHistory(String query){
        this.setWritableDB();

        DateFormat dateFormat = new DateFormat();

        db.execSQL("update searchhistory set date='" + dateFormat.getCurrentTime() +
                "' where word='" + query + "';");

    }

    //search condition
    public void insertInitializeSearchCondition(){
        this.setWritableDB();

        ContentValues values = new ContentValues();

        values.put("idx", 1);
        values.put("search", "서명");
        values.put("sort", "오름차순");

        db.insert("searchcondition", null, values);
    }

    public BookSearchCondition selectSearchCondition(){

        this.setReadableDB();

        BookSearchCondition bookSearchCondition = new BookSearchCondition();

        Cursor c = db.rawQuery("select * from searchcondition;", null);

        while(c.moveToNext()){

            bookSearchCondition.setSortMethod(c.getString(c.getColumnIndex("search")));
            bookSearchCondition.setSort(c.getString(c.getColumnIndex("sort")));

//            Log.i(TAG, bookSearchCondition.getSortMethod() + ", " + bookSearchCondition.getSort());
            return bookSearchCondition;

        }

        return null;

    }

    public void updateTypeCondition(String type){

        this.setWritableDB();

        //Log.i(TAG, "type : " + type);

        db.execSQL("update searchcondition set search='" + type + "' where idx='1';");

    }

    public void updateOrderCondition(String order){

        //Log.i(TAG, "order : " + order);

        db.execSQL("update searchcondition set sort='" + order + "' where idx='1';");

    }

    public void insertInitializeTutorial(){

        this.setWritableDB();

        ContentValues values = new ContentValues();

        values.put("appmain", 1);
        values.put("bookmark", 0);
        values.put("history", 0);
        values.put("searchresult", 0);
        values.put("detail", 0);

        db.insert("tutorial", null, values);

    }

    public List<Integer> selectTutorial(String kind){

        this.setReadableDB();

        List<Integer> indexList = new ArrayList<>();

        Cursor c = db.rawQuery("select " + kind +" from tutorial;", null);

        while(c.moveToNext()){

            indexList.add(c.getInt(c.getColumnIndex(kind)));
            return indexList;

        }

        return null;
    }

    public void updateTutorial(String tutorial){
        db.execSQL("update tutorial set " + tutorial + "='1' where idx='1';");
    }

/*

    db.execSQL("INSERT INTO MONEYBOOK VALUES(null, '" + item + "', " + price + ", '" + create_at + "');");
    db.close();

    public void select() {
        Cursor c = db.rawQuery("select * from mytable;", null);
        while(c.moveToNext()) {
            int id = c.getInt(0);
            String name = c.getString(1);
            Log.d(tag,"id:"+id+",name:"+name);
        }

        c.close();
    }
*/




}
