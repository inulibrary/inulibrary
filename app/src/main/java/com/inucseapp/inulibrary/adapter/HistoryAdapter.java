package com.inucseapp.inulibrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.activity.SearchResultActivity;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.domain.SearchHistory;

import java.util.List;

/**
 * Created by junha on 2017. 8. 24..
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private View fragmentView;

    private Context context;
    private List<SearchHistory> searchHistoryList;

    public HistoryAdapter(Context context, List<SearchHistory> searchHistoryList){
        this.context = context;
        this.searchHistoryList = searchHistoryList;


    }

    public void setFragmentView(View fragmentView) {
        this.fragmentView = fragmentView;
    }

    public void rootViewPushText(String str){
        if(fragmentView != null){
            EditText searchText =(EditText)fragmentView.findViewById(R.id.search_text);
            searchText.setText(str);
            searchText.requestFocus();
            searchText.setSelection(searchText.length());

            InputMethodManager imm = (InputMethodManager)fragmentView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final SearchHistory searchHistory = searchHistoryList.get(position);

        final SQLHandler sqlHandler = new SQLHandler(context);

        DateFormat dateFormat = new DateFormat();
        String date = dateFormat.convertDateForamt(searchHistory.getSearchDate());

        holder.searchWord.setText(searchHistory.getSearchWord());
        holder.searchDate.setText(date);

        holder.pushHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //검색기록 제거
                /*
                int deleteIndex = searchHistoryList.indexOf(searchHistory);
                searchHistoryList.remove(deleteIndex);
                notifyDataSetChanged();

                sqlHandler.deleteHistory(searchHistory);*/

                //검색 프래그먼트의 EditText에 text추가
                rootViewPushText(holder.searchWord.getText().toString());

            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String query = searchHistory.getSearchWord();

                if(sqlHandler.selectHistoryTitle(query) == null){
                    sqlHandler.insertHistory(query);
                }else{
                    sqlHandler.updateHistory(query);
                }

                Intent intent = new Intent(context, SearchResultActivity.class);
                intent.putExtra("query", query);

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchHistoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView searchWord;
        TextView searchDate;
        ImageButton pushHistory;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            searchWord = (TextView) itemView.findViewById(R.id.history_searchword);
            searchDate = (TextView) itemView.findViewById(R.id.history_searchdate);
            pushHistory = (ImageButton) itemView.findViewById(R.id.history_deletebtn);
            cardView = (CardView) itemView.findViewById(R.id.history_cardview);
        }
    }

}
