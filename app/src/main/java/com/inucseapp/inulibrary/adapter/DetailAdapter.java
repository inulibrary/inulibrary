package com.inucseapp.inulibrary.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.domain.Book;

import java.util.List;

/**
 * Created by syg11 on 2017-07-06.
 * Modified by Junha on 2017-08-10.
 *   ListView -> CardView
 */

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder> {

    private Context context;
    private List<Book> bookDetailList;
    private int item_layout;
    private static final String TAG = "DetailAdapter";

    public DetailAdapter(Context context, List<Book> bookDetailList, int item_layout){
        this.context = context;
        this.bookDetailList = bookDetailList;
        this.item_layout = item_layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_detail, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Book book = bookDetailList.get(position);

        holder.bookIDView.setText(book.getBookID());

        if("대출가능".equals(book.getBookState())){
            holder.bookBorrowView.setTextColor(Color.parseColor("#3F51B5"));
        }else{
            holder.bookBorrowView.setTextColor(Color.parseColor("#E91E63"));
        }

        holder.bookBorrowView.setText(book.getBookState());
        holder.bookLocation.setText(book.getBookLocation());


//        Log.i(TAG, "book.getBookReturnDate() : " + book.getBookReturnDate());

        if("".equals(book.getBookReturnDate())){
            holder.bookReturnDate.setVisibility(View.GONE);
        }else {
            holder.bookReturnDate.setText(book.getBookReturnDate());
        }

    }

    @Override
    public int getItemCount() {
        return this.bookDetailList.size();
    }

    //inner class
    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView bookIDView;
        TextView bookBorrowView;
        TextView bookLocation;
        TextView bookReturnDate;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            bookIDView = (TextView) itemView.findViewById(R.id.detail_id);
            bookBorrowView = (TextView) itemView.findViewById(R.id.detail_borrow);
            bookLocation = (TextView) itemView.findViewById(R.id.detail_location);
            bookReturnDate = (TextView) itemView.findViewById(R.id.detail_returndate);
            cardView = (CardView) itemView.findViewById(R.id.detail_cardview);
        }
    }
}
