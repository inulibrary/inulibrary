package com.inucseapp.inulibrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.activity.DetailActivity;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.dataprocessing.ParsingHtmlThread;
import com.inucseapp.inulibrary.domain.Book;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by syg11 on 2017-07-05.
 */

public class DeleteFavoritesAdapter extends RecyclerView.Adapter<DeleteFavoritesAdapter.ViewHolder>{

    private Context context;
    private List<Book> bookList;
    private SweetAlertDialog sweetAlertDialog;

    private static String TAG = "BookmarkAdapter";

    private static final String DETAIL_URL = "http://lib.inu.ac.kr/search/detail/";
    private static final String REGEX = "<table class=\"listtable\" summary.*<\\/td><\\/tr><\\/tbody><\\/table><p><\\/p>";

    private Handler handler;

    private SQLHandler sqlHandler;

    public DeleteFavoritesAdapter(Context context, List<Book> bookList, Handler handler){
        this.context = context;
        this.bookList = bookList;
        this.handler = handler;
        sqlHandler = new SQLHandler(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_delete_favorite, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Book selectBook = bookList.get(position);

        holder.bookTitleView.setText(selectBook.getBookTitle());
        holder.bookIDView.setText(selectBook.getBookID());

        String locationInfo = "학산 : " + selectBook.getBookBorrowableHaksan() +
                              "권,  사범대 : " + selectBook.getBookBorrowableEducation() +
                              "권,  동북아 : " + selectBook.getBookBorrowableSonas() + " 권 대출가능";

        holder.bookNumberOfAvailRentalView.setText(locationInfo);

        DateFormat dateFormat = new DateFormat();

        holder.bookRecentSearchDate.setText(dateFormat.convertDateForamt(selectBook.getSearchDate()) + " 에 검색함.");
        
        holder.bookDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int deleteIndex = bookList.indexOf(selectBook);

                bookList.remove(deleteIndex);
                //notifyItemRemoved(position);
                notifyDataSetChanged();
                sqlHandler.deleteBookmarkInfo(selectBook);
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("bookinfo", selectBook);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {

        if(this.bookList == null){
            return 0;
        }else{
            return this.bookList.size();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView bookTitleView;
        TextView bookIDView;
        TextView bookNumberOfAvailRentalView;
        TextView bookRecentSearchDate;
        ImageButton bookRefreshBtn;
        ImageButton bookDeleteBtn;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            bookTitleView = (TextView) itemView.findViewById(R.id.bookmark_title);
            bookIDView = (TextView) itemView.findViewById(R.id.bookmark_id);
            bookNumberOfAvailRentalView = (TextView) itemView.findViewById(R.id.bookmark_avail_rent);
            bookRecentSearchDate = (TextView) itemView.findViewById(R.id.bookmark_recent_search);
            bookDeleteBtn = (ImageButton) itemView.findViewById(R.id.bookmark_deletebtn);
            cardView = (CardView) itemView.findViewById(R.id.bookmark_cardview);
        }

    }

}
