package com.inucseapp.inulibrary.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.inucseapp.inulibrary.R;
import com.inucseapp.inulibrary.activity.SearchResultActivity;
import com.inucseapp.inulibrary.database.SQLHandler;
import com.inucseapp.inulibrary.dataprocessing.DateFormat;
import com.inucseapp.inulibrary.domain.SearchHistory;

import java.util.List;

/**
 * Created by junha on 2017. 8. 24..
 */

public class DeleteHistoryAdapter extends RecyclerView.Adapter<DeleteHistoryAdapter.ViewHolder> {
    private View fragmentView;

    private Context context;
    private List<SearchHistory> searchHistoryList;

    public DeleteHistoryAdapter(Context context, List<SearchHistory> searchHistoryList){
        this.context = context;
        this.searchHistoryList = searchHistoryList;


    }

    public void setFragmentView(View fragmentView) {
        this.fragmentView = fragmentView;
    }

    public void rootViewPuchText(String str){
        if(fragmentView != null){
            ((EditText)fragmentView.findViewById(R.id.search_text)).setText(str);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_delete_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final SearchHistory searchHistory = searchHistoryList.get(position);

        final SQLHandler sqlHandler = new SQLHandler(context);

        DateFormat dateFormat = new DateFormat();
        String date = dateFormat.convertDateForamt(searchHistory.getSearchDate());

        holder.searchWord.setText(searchHistory.getSearchWord());
        holder.searchDate.setText(date);

        holder.pushHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //검색기록 제거

                int deleteIndex = searchHistoryList.indexOf(searchHistory);
                searchHistoryList.remove(deleteIndex);
                notifyDataSetChanged();

                sqlHandler.deleteHistory(searchHistory);

                //검색 프래그먼트의 EditText에 text추가
                //rootViewPuchText(holder.searchWord.getText().toString());

            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String query = searchHistory.getSearchWord();

                if(sqlHandler.selectHistoryTitle(query) == null){
                    sqlHandler.insertHistory(query);
                }else{
                    sqlHandler.updateHistory(query);
                }

                Intent intent = new Intent(context, SearchResultActivity.class);
                intent.putExtra("query", query);

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchHistoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView searchWord;
        TextView searchDate;
        ImageButton pushHistory;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            searchWord = (TextView) itemView.findViewById(R.id.history_searchword);
            searchDate = (TextView) itemView.findViewById(R.id.history_searchdate);
            pushHistory = (ImageButton) itemView.findViewById(R.id.delete_history_btn);
            cardView = (CardView) itemView.findViewById(R.id.history_cardview);
        }
    }

}
