package com.inucseapp.inulibrary.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.inucseapp.inulibrary.R;

/**
 * Created by junha on 2017. 9. 7..
 */

public class BookmarkTutorial extends AppIntro {

    int backgroundColor = 0xFF3F51B5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("관심도서를 한눈에 확인할 수 있습니다.");
        sliderPage1.setDescription("청구기호와 대출가능한 도서가 몇권인지 한눈에 확인할 수 있습니다.");
        sliderPage1.setImageDrawable(R.drawable.tutorial_bookmark_01);
        sliderPage1.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("책이 반납되었는지 궁금하신가요?");
        sliderPage2.setDescription("새로고침을 눌러 대출정보를 다시 가져올 수 있습니다.");
        sliderPage2.setImageDrawable(R.drawable.tutorial_bookmark_02);
        sliderPage2.setBgColor(backgroundColor);
        addSlide(AppIntroFragment.newInstance(sliderPage2));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
