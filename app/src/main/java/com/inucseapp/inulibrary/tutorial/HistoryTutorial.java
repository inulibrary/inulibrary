package com.inucseapp.inulibrary.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.inucseapp.inulibrary.R;

/**
 * Created by junha on 2017. 9. 6..
 */

public class HistoryTutorial extends AppIntro {

    int backgroundColor = 0xFF3F51B5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("검색 조건을 설정해보세요.");
        sliderPage1.setDescription("설정한 조건은 저장되어 다음에 검색할 때도 유지됩니다.");
        sliderPage1.setBgColor(backgroundColor);
        sliderPage1.setImageDrawable(R.drawable.tutorial_history_01);
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("이전에 검색한 내용을 다시 검색하고 싶으신가요?");
        sliderPage2.setDescription("한 번 검색한 검색어는 저장되어, 단 한번의 클릭으로 다시 검색할 수 있습니다.");
        sliderPage2.setBgColor(backgroundColor);
        sliderPage2.setImageDrawable(R.drawable.tutorial_history_02);
        addSlide(AppIntroFragment.newInstance(sliderPage2));

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
