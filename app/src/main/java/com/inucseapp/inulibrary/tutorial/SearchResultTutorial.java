package com.inucseapp.inulibrary.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;
import com.inucseapp.inulibrary.R;

/**
 * Created by junha on 2017. 9. 6..
 */

public class SearchResultTutorial extends AppIntro {

    int backgroundColor = 0xFF3F51B5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("상세정보를 볼 수 있습니다!");
        sliderPage1.setDescription("검색결과를 선택해보세요.");
        sliderPage1.setBgColor(backgroundColor);
        sliderPage1.setImageDrawable(R.drawable.tutorial_search_01);
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("화면을 아래로 내려보세요.");
        sliderPage2.setDescription("다음 페이지를 불러올 수 있습니다.");
        sliderPage2.setBgColor(backgroundColor);
        sliderPage2.setImageDrawable(R.drawable.tutorial_search_02);
        addSlide(AppIntroFragment.newInstance(sliderPage2));

    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        finish();
    }
}
